package com.learning.scd.histryxdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableHystrixDashboard
public class HistryxDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(HistryxDashboardApplication.class, args);
	}

}
