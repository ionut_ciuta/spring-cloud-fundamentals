package com.learning.scd.ribbonclientwitheureka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@EnableDiscoveryClient
//@RibbonClient(name = "ribbon-service")
@EnableCircuitBreaker
@SpringBootApplication
@RestController
public class RibbonClientWithEurekaApplication {

	@Autowired
	private TestService testService;

	public static void main(String[] args) {
		SpringApplication.run(RibbonClientWithEurekaApplication.class, args);
	}

	@GetMapping("/ribbon_test")
	public String getMessageFromRibbonService() {
		return testService.get();
	}
}
;