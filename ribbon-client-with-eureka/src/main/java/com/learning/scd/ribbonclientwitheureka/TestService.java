package com.learning.scd.ribbonclientwitheureka;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class TestService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand
    public String get() {
        return restTemplate.exchange(
                "http://ribbon-service/ribbon_test",
                HttpMethod.GET,
                null,
                String.class,
                Map.of()
        ).getBody();
    }
}
