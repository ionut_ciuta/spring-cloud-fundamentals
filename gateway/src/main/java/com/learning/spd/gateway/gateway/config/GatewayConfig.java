package com.learning.spd.gateway.gateway.config;

import com.learning.spd.gateway.gateway.filters.AddRequestHeaderPreFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Bean
    public AddRequestHeaderPreFilter getAddHeaderPostFilter() {
        return new AddRequestHeaderPreFilter();
    }
}
