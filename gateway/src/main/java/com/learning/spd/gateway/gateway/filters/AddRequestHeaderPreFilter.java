package com.learning.spd.gateway.gateway.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class AddRequestHeaderPreFilter extends ZuulFilter {
    private static final String FILTER_TYPE_PRE = "pre";

    @Override
    public String filterType() {
        // run before
        return FILTER_TYPE_PRE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        // always filter
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader("x-location", "Transylvania");

        // return value not used, null is fine
        return null;
    }
}
