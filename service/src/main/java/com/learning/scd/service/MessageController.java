package com.learning.scd.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @Value("${demo.message}")
    private String message;

    @GetMapping("${demo.apiPath}")
    public String getMessage(@RequestHeader(value = "x-location", required = false, defaultValue = "") String location) {
        return message + " " + location;
    }
}
